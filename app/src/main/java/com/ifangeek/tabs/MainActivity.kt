package com.ifangeek.tabs

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.ifangeek.tabs.fragments.ShoppingFragment
import com.ifangeek.tabs.fragments.VideosFragment
import com.ifangeek.tabs.fragments.WebFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initButtons()
        prepareUI()
        initDefaultFragment()

        Log.d("DEBUG",MainActivity::class.java.simpleName)
    }

    private fun initDefaultFragment(){
        changeFragment(WebFragment.newInstance())
        buttonWeb.setCompoundDrawablesRelativeWithIntrinsicBounds(
            null,
            null,
            null,
            ContextCompat.getDrawable(this, R.drawable.shape_line)
        )
    }
    private fun prepareUI() {
        buttonWeb.setOnClickListener {
            updateButton(it)
            initFragments(it)
        }

        buttonShopping.setOnClickListener {
            updateButton(it)
            initFragments(it)
        }

        buttonVideos.setOnClickListener {
            updateButton(it)
            initFragments(it)
        }
    }

    private fun initButtons() {
        buttonShopping.setCompoundDrawablesWithIntrinsicBounds(
            null, null, null, ContextCompat.getDrawable(
                this,
                R.drawable.shape_line_normal
            )
        )

        buttonVideos.setCompoundDrawablesRelativeWithIntrinsicBounds(
            null,
            null,
            null,
            ContextCompat.getDrawable(this, R.drawable.shape_line_normal)
        )

        buttonWeb.setCompoundDrawablesRelativeWithIntrinsicBounds(
            null,
            null,
            null,
            ContextCompat.getDrawable(this, R.drawable.shape_line_normal)
        )
    }

    private fun updateButton(view: View?) {



        when (view?.id) {
            R.id.buttonWeb -> {
                buttonWeb.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    null,
                    null,
                    null,
                    ContextCompat.getDrawable(this, R.drawable.shape_line)
                )

                buttonShopping.setCompoundDrawablesWithIntrinsicBounds(
                    null, null, null, ContextCompat.getDrawable(
                        this,
                        R.drawable.shape_line_normal
                    )
                )

                buttonVideos.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    null,
                    null,
                    null,
                    ContextCompat.getDrawable(this, R.drawable.shape_line_normal)
                )


            }
            R.id.buttonShopping -> {
                buttonWeb.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    null,
                    null,
                    null,
                    ContextCompat.getDrawable(this, R.drawable.shape_line_normal)
                )

                buttonShopping.setCompoundDrawablesWithIntrinsicBounds(
                    null, null, null, ContextCompat.getDrawable(
                        this,
                        R.drawable.shape_line
                    )
                )

                buttonVideos.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    null,
                    null,
                    null,
                    ContextCompat.getDrawable(this, R.drawable.shape_line_normal)
                )
            }
            R.id.buttonVideos -> {
                buttonWeb.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    null,
                    null,
                    null,
                    ContextCompat.getDrawable(this, R.drawable.shape_line_normal)
                )

                buttonShopping.setCompoundDrawablesWithIntrinsicBounds(
                    null, null, null, ContextCompat.getDrawable(
                        this,
                        R.drawable.shape_line_normal
                    )
                )

                buttonVideos.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    null,
                    null,
                    null,
                    ContextCompat.getDrawable(this, R.drawable.shape_line)
                )
            }
        }
    }

    private fun initFragments(view:View?){
        val fragment = when(view?.id){
            R.id.buttonWeb -> {
                WebFragment.newInstance()
            }
            R.id.buttonShopping -> {
                ShoppingFragment.newInstance()
            }
            R.id.buttonVideos -> {
                VideosFragment.newInstance()
            }
            else -> null
        }

        fragment?.also(::changeFragment)

    }

    private fun changeFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fLayoutContainer, fragment)
            commit()
        }
    }


}